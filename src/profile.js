import DiscoverySettings from './discoverySettings';

export default function profile() {
    const getProfile = () => {
        return new Promise((resolve, reject) => {
            this.connection.get('profile',
                { },
                function(err, res, body){
                    if (err) reject(err);
                    resolve(body);
                });
        });
    };

    const setBio = (newBio) => {
        return new Promise((resolve, reject) => {
            this.connection.post('profile',
                { bio: newBio },
                function(err, res, body){
                    if (err) reject(err);
                    resolve(body);
                });
        });
    };

    const setMale = () => {
        return new Promise((resolve, reject) => {
            this.connection.post('profile',
                { "gender": 0 }, // verify this value
                function(err, res, body){
                    if (err) reject(err);
                    resolve(body);
                });
        })
    };

    const setFemale = () => {
        return new Promise((resolve, reject) => {
            this.connection.post('profile',
                { "gender": 1 }, // verify this value
                function(err, res, body){
                    if (err) reject(err);
                    resolve(body);
                });
        })
    };

    const setDiscoverySettings = (
            discoverable,
            gender_filter,
            age_filter_min,
            age_filter_max,
            distance_filter,
            squads_discoverable,
            squads_only
        ) => {
        let settings = new DiscoverySettings(discoverable, gender_filter, age_filter_min, age_filter_max, distance_filter, squads_discoverable, squads_only);
        return new Promise((resolve, reject) => {
            this.connection.post('profile',
                {
                    "discoverable": settings.discoverable,
                    "gender_filter": settings.gender_filter, // validation on these values
                    "age_filter_min": settings.age_filter_min, // this.model.value
                    "age_filter_max": settings.age_filter_max,
                    "distance_filter": settings.distance_filter,
                    "squads_discoverable": settings.squads_discoverable,
                    "squads_only": settings.squads_only
                },
                function(err, res, body){
                    if (err) reject(err);
                    resolve(body);
                });
        })
    };

    return {
        getProfile,
        setBio,
        setMale,
        setFemale,
        setDiscoverySettings
    };
}
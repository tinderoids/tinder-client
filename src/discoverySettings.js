export default class DiscoverySettings {
    constructor(discoverable, gender_filter, age_filter_min, age_filter_max, distance_filter, squads_discoverable, squads_only) {
        this.discoverable = discoverable;
        this.gender_filter = gender_filter;
        this.age_filter_min = age_filter_min;
        this.age_filter_max = age_filter_max;
        this.distance_filter = distance_filter;
        this.squads_discoverable = squads_discoverable;
        this.squads_only = squads_only;
    }

    set discoverable(discoverable) {
        //if (discoverable != Boolean) throw new TypeError('Property: discoverable must be of type Boolean');
        this.discoverable = discoverable;
    }

    set gender_filter(gender_filter) {
        // -1 M+F
        // 0 M
        // 1 F
        //if (gender_filter != Integer) throw new TypeError('Property: gender_filter must be of type Integer');
        this.gender_filter = gender_filter;
    }

    set age_filter_min(age_filter_min) {
        // 18 = 18
        //if (age_filter_min != Integer) throw new TypeError('Property: age_filter_min must be of type Integer');
        //if (age_filter_min < 18) throw new RangeError('Property: age_filter_min must be >= 18 ... paedo');
        this.age_filter_min = age_filter_min;
    }

    set age_filter_max(age_filter_max) {
        // 54 = 54
        // 55+ = 1000
        //if (age_filter_max != Integer) throw new TypeError('Property: age_filter_max must be of type Integer');
        //if (age_filter_max > 55) throw new RangeError('Property: age_filter_max must be <= 55');
        this.age_filter_max = age_filter_max;
    }

    set distance_filter(distance_filter) {
        // need to check the settings for km/mi before validating this field
        // 2-161km
        // 2 km = 1
        // 161+ km = 100
        // 1 mi = 1
        // 100 mi = 100
        // 0.62mi to the km ... duhhh
        //if (age_filter_max > 55) throw new RangeError('Property: age_filter_max must be <= 55');
        //if (age_filter_max > 55) throw new RangeError('Property: age_filter_max must be <= 55');
        this.distance_filter = distance_filter;
    }

    set squads_discoverable(squads_discoverable) {
        //if (squads_discoverable != Boolean) throw new TypeError('Property: squads_discoverable must be of type Boolean');
        this.squads_discoverable = squads_discoverable;
    }

    set squads_only(squads_only) {
        //if (squads_only != Boolean) throw new TypeError('Property: squads_only must be of type Boolean');
        this.squads_only = squads_only;
    }
    
    get discoverable() {
        return this.discoverable;
    }

    get gender_filter() {
        return this.gender_filter;
    }

    get age_filter_min() {
        return this.age_filter_min;
    }

    get age_filter_max() {
        return this.age_filter_max;
    }

    get distance_filter() {
        return this.distance_filter;
    }

    get squads_discoverable() {
        return this.squads_discoverable;
    }

    get squads_only() {
        return this.squads_only;
    }
}

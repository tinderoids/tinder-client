import * as request from 'request-json';
import 'babel-polyfill';


export class fb {
    constructor(id, token, locale) {
        this.id = id;
        this.token = token;
        this.locale = locale;
    }
}

export class auth {

    constructor(fb) {
        this.fbID = fb.id;
        this.fbToken = fb.token;
        this.locale = fb.locale;
        this.token = undefined;
        this.client = request.createClient('https://api.gotinder.com', {
            headers: { // required?
                'user-agent': 'Tinder Android Version 5.0.5', // yes? User-Agent: Tinder/3.0.4 (iPhone; iOS 7.1; Scale/2.00) etc
                'os-version': '23', // no
                'content-type':	'application/json; charset=utf-8', // yes?
                'app-version': '1204', // no
                'platform': 'android' // no
            }
        });
    }

    postCredentials() {
        return new Promise((resolve, reject) => {
                this.client.post('auth',
                    { 'locale': this.locale, 'facebook_id': this.fbID, 'facebook_token': this.fbToken },
                    function(err, res, body){
                        if (err) reject(err);
                        if (body.code == 401) reject(body);
                        resolve(body.token);
                    });
            }).catch((error) => {
                throw(error)
            });
    }

    getToken() {
        if(this.token !== undefined) {
            console.warn('Authenticating unnecessarily.')
            return this;
        } else {
            return this.postCredentials()
                .catch(error => {
                    console.error('Error: ' + JSON.stringify(error, null, 4));
                    throw new TypeError('Invalid facebook Token & ID combination.')
                })
                .then(token => {
                    console.log('Authenticating.')
                    this.token = token;
                    this.client.setToken(this.token);
                });
        }
    }

}
